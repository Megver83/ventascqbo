module org.ventascqbo {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    exports org.ventascqbo;
    opens org.ventascqbo.controller;
}
