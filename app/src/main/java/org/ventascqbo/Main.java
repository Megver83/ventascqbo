package org.ventascqbo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
* Aplicación para comprar y vender.
*
* @author  David Pizarro, Tomás Vargas
* @version 1.0
*/
public class Main extends Application {
    public static RegistroUsuarios usersRegistry = new RegistroUsuarios("data/RegistroUsuarios.txt");

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/Login.fxml")));
        Scene scene = new Scene(Objects.requireNonNull(root));

        stage.setTitle("Ventas Coquimbo - Inicio");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
