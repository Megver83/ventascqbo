package org.ventascqbo;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
* Superclase para leer archivos y realizar acciones de lectura y escritura de textos.
*/
abstract class TextReader {
    protected ArrayList<String[]> matrixArrayList = new ArrayList<String[]>();
    protected String txtFile;

    /**
     * Crea una matriz con los datos de usuario.
     *
     * @throws IOException En errores de entrada.
     */
    public TextReader(String txtFile) {
        this.txtFile = txtFile;
        try {
            File fileObj = new File(txtFile);
            if(!fileObj.exists()) {
                System.out.println(txtFile + " no se encontró, se va a crear como achivo vacío...");
                fileObj.createNewFile();
                return;
            }
            Scanner scanObj = new Scanner(fileObj);

            // Crea la matriz
            while (scanObj.hasNextLine()) {
                String[] data = scanObj.nextLine().split(";");
                matrixArrayList.add(data);
            }
            scanObj.close();
        } catch(Exception e) {
            e.printStackTrace();
            System.out.println("Ocurrió un error al intentar acceder al archivo.");
        }
    }

    /**
    * Añade un nuevo dato.
    *
    * @param data Un array de String.
    */
    public void add(String[] data) {
        matrixArrayList.add(data);
    }

    /**
     * Escribe los datos de una matriz a un archivo de texto, sin importar si existe o no desde antes.
     *
     * @throws IOException En errores de entrada.
     */
    public void writeTo() throws IOException {
        File fileObj = new File(txtFile);
        FileWriter file = new FileWriter(fileObj);
        String end;

        for(String[] lista : matrixArrayList) {
            for(int i = 0; i < lista.length; i++) {
                if(i == lista.length - 1) {
                    end = "\n";
                } else {
                    end = ";";
                }
                file.write(lista[i] + end);
            }
        }
        file.close();
    }
}
