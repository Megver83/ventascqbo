package org.ventascqbo.controller;

import org.ventascqbo.TableData;
import java.util.ArrayList;

/**
 * Crea objetos estáticos para tener datos globalmente accesibles y modificables.
 */
public class Common {
    /**
     * true si hay que escribir la tabla al iniciar PerfilController.
     * Las tablas tableArrayList y myPostsList deben escribirse solo una vez al inicio con
     * los datos de Productos.txt, por esto PerfilController lo cambia a 'false'.
     */
    static boolean writeTable = true;

    /**
     * true si se ha realizado una compra, false en caso contrario.
     * De este modo, PerfilController.initialize() mostrará una notificación si corresponde.
     */
    static boolean purchaseDone = false;

    /**
     * Objeto TableData con los datos del vendedor asociado al producto seleccionado.
     */
    static TableData sellerTableData;

    /**
     * Objeto TableData con los datos del producto seleccionado.
     */
    static TableData productTableData;

    /**
     * Objeto TableData con los datos del usuario en la sesión actual.
     */
    static TableData myDataTableData;

    // Listas con Mis Compras, Mis Publicaciones y Productos Disponisbles, respectivamente.
    static ArrayList<TableData> myShoppingList = new ArrayList<TableData>();
    static ArrayList<TableData> myPostsList = new ArrayList<TableData>();
    static ArrayList<TableData> tableArrayList = new ArrayList<TableData>();

    /**
     * Array con las categorías de productos.
     */
    static String[] categories = {
        "Deporte",
        "Música",
        "Juegos",
        "Hogar"
    };

    /**
     * Índice del objeto TableData en myPostsList, si es menor que 0 entonces es para publicar.
     * Cuando se desea editar un producto, toma un valor igual o mayor que cero.
     */
    static int editProductIndex = -1;

    /**
     * Añade el producto a myShoppingList y lo borra de tableArrayList.
     * Esto debe ser llamado desde InfoProductos si se realiza la compra.
     */
    static void buyProduct() {
        for(int i = 0; i < tableArrayList.size(); i++) {
            // Elimina el objeto de tabla (fila) seleccionado en tableArrayList
            if(tableArrayList.get(i) == productTableData) {
                myShoppingList.add(productTableData);
                purchaseDone = true;

                tableArrayList.remove(i);
                break;
            }
        }
    }
}
