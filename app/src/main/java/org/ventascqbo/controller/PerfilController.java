package org.ventascqbo.controller;

import org.ventascqbo.Main;
import org.ventascqbo.TableData;
import org.ventascqbo.ReadProducts;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.FXCollections;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;

import java.io.IOException;
import java.util.Objects;

public class PerfilController {
    /**
     * ArrayList con los datos de tabla para mostrar.
     * Se modifica en función del filtro que se aplique.
     */
    private ArrayList<TableData> tableDisplayArrayList = new ArrayList<TableData>();

    /**
     * Indice de tableDisplayArrayList.
     * Obtiene un valor cuando se hace clic en alguna fila de la tabla.
     */
    private int tableChoice;

    private ReadProducts productsObj = new ReadProducts("data/Productos.txt");

    /**
     * Array de categorías, igual que Common.categories pero la primera opción es "Todos".
     */
    private String[] categories = new String[Common.categories.length + 1];

    @FXML
    private ChoiceBox<String> categoryChoiceBox;

    @FXML
    private TableView<TableData> productsTableView;

    @FXML
    private TableColumn<TableData, String> categoryTableColumn;

    @FXML
    private TableColumn<TableData, String> dateTableColumn;

    @FXML
    private TableColumn<TableData, String> idTableColumn;

    @FXML
    private TableColumn<TableData, String> nameTableColumn;

    @FXML
    private TableColumn<TableData, String> seenTableColumn;

    @FXML
    private Label welcomeLabel;

    @FXML
    private Label purchaseLabel;

    public void initialize() {
        welcomeLabel.setText("Bienvenido, " + Common.myDataTableData.getCol1());

        // Concatena el array Common.categories para añadirle la opción "Todos"
        System.arraycopy(new String[]{"Todos"}, 0, categories, 0, 1);
        System.arraycopy(Common.categories, 0, categories, 1, Common.categories.length);

        if(Common.purchaseDone) {
            purchaseLabel.setText("¡Tu compra de " + Common.productTableData.getCol2() + " fue realizada con éxito!");
            Common.purchaseDone = false;
        }

        // Añade ChoiceBox y deja seleccionada la primera opción por defecto
        categoryChoiceBox.getItems().addAll(categories);
        categoryChoiceBox.getSelectionModel().selectFirst();

        // Escribe la tabla
        if(Common.writeTable) {
            for(String[] line : productsObj.get()) {
                if(line[6].equals(Common.myDataTableData.getCol1())) {
                    Common.myPostsList.add(new TableData(line));
                } else {
                    Common.tableArrayList.add(new TableData(line));
                }
            }
            Common.writeTable = false;
        }
        tableDisplayArrayList.addAll(Common.tableArrayList);

        // Acción a ejecutar cuando se selecciona un filtro.
        categoryChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue ov, Number value, Number new_value) {
                // Indice de 'categories'
                int choice = new_value.intValue();

                // Limpia la tabla a mostrar para luego rellenar
                tableDisplayArrayList.clear();

                if(choice == 0) {
                    tableDisplayArrayList.addAll(Common.tableArrayList);
                } else {
                    for(int i = 0; i < Common.tableArrayList.size(); i++ ) {
                        TableData tableObj = Common.tableArrayList.get(i);

                        if(tableObj.getCol3().equals(categories[choice])) {
                            tableDisplayArrayList.add(tableObj);
                        }
                    }
                }
                productsTableView.setItems(FXCollections.observableArrayList(tableDisplayArrayList));
            }
        });

        // Acción a ejecutar cuando se selecciona una fila de la tabla.
        productsTableView.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue ov, Number value, Number new_value) {
                tableChoice = new_value.intValue();
            }
        });

        // Define que columna de TableData le corresponde a los objetos TableColumn
        idTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col1"));
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col2"));
        categoryTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col3"));
        dateTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col4"));
        seenTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col9"));

        // Escribe los datos en la tabla
        productsTableView.setItems(FXCollections.observableArrayList(tableDisplayArrayList));
    }

    @FXML
    void myBuyButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/InfoCompras.fxml")));
        Stage purchasesInfoStage = new Stage();
        Scene scene = new Scene(Objects.requireNonNull(root));

        purchasesInfoStage.setTitle("Mis publicaciones");
        purchasesInfoStage.setScene(scene);
        purchasesInfoStage.show();
    }

    @FXML
    void myUploadsButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/MisPublicaciones.fxml")));
        Stage uploadsStage = new Stage();
        Scene scene = new Scene(Objects.requireNonNull(root));

        uploadsStage.setTitle("Información de compras");
        uploadsStage.setScene(scene);
        uploadsStage.show();
    }

    @FXML
    void productInfoButton(ActionEvent event) throws IOException {
        for(int i = 0; i < Common.tableArrayList.size(); i++) {
            // Deja en visto el objeto de tabla (fila) seleccionado en Common.tableArrayList
            if(tableDisplayArrayList.get(tableChoice) == Common.tableArrayList.get(i)) {
                Common.tableArrayList.get(i).set(8, "Si");
                break;
            }
        }

        // Actualiza variables globales. Utilizaremos esta info en InfoProductoController
        Common.productTableData = tableDisplayArrayList.get(tableChoice);
        Common.sellerTableData = Main.usersRegistry.get(Common.productTableData.getCol7());

        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/InfoProducto.fxml")));
        Stage productInfoStage = new Stage();
        Scene scene = new Scene(Objects.requireNonNull(root));

        productInfoStage.setTitle("Información del producto");
        productInfoStage.setScene(scene);

        Stage stage = (Stage) welcomeLabel.getScene().getWindow();
        stage.close();

        productInfoStage.show();
    }

    @FXML
    void sellProductButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/PublicarProducto.fxml")));
        Stage uploadsStage = new Stage();
        Scene scene = new Scene(Objects.requireNonNull(root));

        uploadsStage.setTitle("Publicar producto");
        uploadsStage.setScene(scene);
        uploadsStage.show();
    }

}
