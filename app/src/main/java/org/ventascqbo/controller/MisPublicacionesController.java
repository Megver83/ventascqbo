package org.ventascqbo.controller;

import org.ventascqbo.TableData;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.FXCollections;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;

import java.io.IOException;
import java.util.Objects;

public class MisPublicacionesController {
    // Indice de Common.myPostsList, se usará en PublicarProductoController para saber qué fila modificar
    private int tableChoice;

    @FXML
    private TableColumn<TableData, String> categoryTableColumn;

    @FXML
    private TableColumn<TableData, String> dateTableColumn;

    @FXML
    private Label fullnameLabel;

    @FXML
    private TableColumn<TableData, String> idTableColumn;

    @FXML
    private Label personalInfoLabel;

    @FXML
    private TableView<TableData> postsTableView;

    @FXML
    private Label sellsNumberLabel;

    @FXML
    private TableColumn<TableData, String> soldTableColumn;

    @FXML
    private TableColumn<TableData, String> productNameTableColumn;

    public void initialize() {
        sellsNumberLabel.setText(Common.myDataTableData.getCol5());

        fullnameLabel.setText(Common.myDataTableData.getCol2());
        personalInfoLabel.setText(
            Common.myDataTableData.getCol1() + " <" + Common.myDataTableData.getCol3() + ">" + "\n" +
            Common.myDataTableData.getCol4()
        );

        postsTableView.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue ov, Number value, Number new_value) {
                tableChoice = new_value.intValue();
            }
        });

        // Define que columna de TableData le corresponde a los objetos TableColumn
        idTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col1"));
        productNameTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col2"));
        categoryTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col3"));
        dateTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col4"));
        soldTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col8"));

        // Escribe los datos en la tabla
        postsTableView.setItems(FXCollections.observableArrayList(Common.myPostsList));
    }

    @FXML
    void deleteButton(ActionEvent event) {
        Common.myPostsList.remove(tableChoice);
        postsTableView.setItems(FXCollections.observableArrayList(Common.myPostsList));
    }

    @FXML
    void editButton(ActionEvent event) throws IOException {
        // Actualiza variable global
        Common.editProductIndex = tableChoice;

        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/PublicarProducto.fxml")));
        Stage uploadsStage = new Stage();
        Scene scene = new Scene(Objects.requireNonNull(root));

        uploadsStage.setTitle("Editar producto");
        uploadsStage.setScene(scene);

        // Cierra esta ventana
        Stage stage = (Stage) fullnameLabel.getScene().getWindow();
        stage.close();

        uploadsStage.show();
    }
}
