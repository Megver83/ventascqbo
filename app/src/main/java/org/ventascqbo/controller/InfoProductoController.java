package org.ventascqbo.controller;

import org.ventascqbo.TableData;
import org.ventascqbo.ReadProducts;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.util.ArrayList;
import javafx.collections.FXCollections;

import java.io.IOException;
import java.util.Objects;

public class InfoProductoController {
    private String productId = Common.productTableData.getCol1();
    private ReadProducts productOffers = new ReadProducts("data/id/" + productId + ".txt");
    private ArrayList<TableData> productOffersArrayList = new ArrayList<TableData>();

    @FXML
    private Label contactLabel;

    @FXML
    private TableColumn<TableData, String> dateTableColumn;

    @FXML
    private Label descLabel;

    @FXML
    private TableColumn<TableData, String> idTableColumn;

    @FXML
    private Label priceLabel;

    @FXML
    private ImageView productImageView;

    @FXML
    private Label productNameLabel;

    @FXML
    private TableView<TableData> productTableView;

    @FXML
    private Label sellerInfoLabel;

    @FXML
    private Label sellerNameLabel;

    @FXML
    private TableColumn<TableData, String> usernameTableColumn;

    public void initialize() {
        // Escribe la tabla
        for(String[] line : productOffers.get()) {
            productOffersArrayList.add(new TableData(line));
        }

        // Define que columna de TableData le corresponde a los objetos TableColumn
        idTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col1"));
        dateTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col2"));
        usernameTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col3"));

        // Escribe los datos en la tabla
        productTableView.setItems(FXCollections.observableArrayList(productOffersArrayList));

        productImageView.setImage(
            new Image(
                Objects.requireNonNull(getClass().getResourceAsStream("/img/" + productId + ".jpg"))
            )
        );

        productNameLabel.setText(Common.productTableData.getCol2());
        priceLabel.setText(Common.productTableData.getCol5());
        descLabel.setText(Common.productTableData.getCol6());
        sellerNameLabel.setText(Common.productTableData.getCol7());
        contactLabel.setText(Common.sellerTableData.getCol4());
        sellerInfoLabel.setText(Common.sellerTableData.getCol2() + "\n<" + Common.sellerTableData.getCol3() + ">");
    }

    @FXML
    void goBackButton(ActionEvent event) throws IOException {
        openProfileScene();
    }

    @FXML
    void buyButton(ActionEvent event) throws IOException {
        Common.buyProduct();
        openProfileScene();
    }

    void openProfileScene() throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/Perfil.fxml")));
        Stage registerStage = new Stage();
        Scene scene = new Scene(Objects.requireNonNull(root));

        registerStage.setTitle("Ventas Coquimbo - Perfil");
        registerStage.setScene(scene);

        // Cierra esta ventana antes de ir al perfil
        Stage loginStage = (Stage) productNameLabel.getScene().getWindow();
        loginStage.close();

        registerStage.show();
    }
}
