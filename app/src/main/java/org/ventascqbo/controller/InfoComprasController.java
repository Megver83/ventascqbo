package org.ventascqbo.controller;

import org.ventascqbo.TableData;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.FXCollections;

public class InfoComprasController {

    @FXML
    private TableColumn<TableData, String> idTableColumn;

    @FXML
    private TableColumn<TableData, String> priceTableColumn;

    @FXML
    private TableColumn<TableData, String> productNameTableColumn;

    @FXML
    private Label purchasesNumberLabel;

    @FXML
    private TableView<TableData> purchasesTableView;

    @FXML
    private TableColumn<TableData, String> usernameTableColumn;

    public void initialize() {
        purchasesNumberLabel.setText(Integer.toString(Common.myShoppingList.size()));

        // Define que columna de TableData le corresponde a los objetos TableColumn
        idTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col1"));
        usernameTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col7"));
        productNameTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col2"));
        priceTableColumn.setCellValueFactory(new PropertyValueFactory<TableData, String>("col5"));

        // Escribe los datos en la tabla
        purchasesTableView.setItems(FXCollections.observableArrayList(Common.myShoppingList));
    }
}
