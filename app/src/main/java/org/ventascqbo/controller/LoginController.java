package org.ventascqbo.controller;

import org.ventascqbo.Main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class LoginController {

    @FXML
    private Label loginMessageLabel;

    @FXML
    private PasswordField passwordPasswordField;

    @FXML
    private TextField userTextField;

    @FXML
    void handleButtonAction(ActionEvent event) throws IOException {
        if(Main.usersRegistry.verify(userTextField.getText(), passwordPasswordField.getText())) {
            loginMessageLabel.setText("");
            Common.myDataTableData = Main.usersRegistry.get(userTextField.getText());
        } else {
            loginMessageLabel.setText("¡Usuario o contraseña incorrecta!");
            return;
        }

        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/Perfil.fxml")));
        Stage registerStage = new Stage();
        Scene scene = new Scene(Objects.requireNonNull(root));

        registerStage.setTitle("Ventas Coquimbo - Perfil");
        registerStage.setScene(scene);

        // Cierra esta ventana antes de ir al perfil
        Stage loginStage = (Stage) userTextField.getScene().getWindow();
        loginStage.close();

        registerStage.show();
    }

    @FXML
    void registroHyperlink(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/Register.fxml")));
        Stage registerStage = new Stage();
        Scene scene = new Scene(Objects.requireNonNull(root));

        registerStage.setTitle("Registrarse");
        registerStage.setScene(scene);
        registerStage.show();
    }
}
