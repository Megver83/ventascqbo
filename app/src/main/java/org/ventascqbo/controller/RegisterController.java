package org.ventascqbo.controller;

import org.ventascqbo.Main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import java.io.IOException;

public class RegisterController {

    @FXML
    private PasswordField confirmPasswordField;

    @FXML
    private TextField contactTextField;

    @FXML
    private TextField emailTextField;

    @FXML
    private TextField fullNameTextField;

    @FXML
    private PasswordField newPasswordField;

    @FXML
    private TextField newUserTextField;

    @FXML
    private Label warnTextLabel;

    @FXML
    private Label congratsTextLabel;

    @FXML
    void registerButton(ActionEvent event) throws IOException {
        if(
            newUserTextField.getText().isBlank() ||
            newPasswordField.getText().isBlank() ||
            fullNameTextField.getText().isBlank() ||
            emailTextField.getText().isBlank() ||
            contactTextField.getText().isBlank() ||
            confirmPasswordField.getText().isBlank()
        ) {
            warnTextLabel.setText("Por favor, complete todos los datos.");
            return;
        }

        if(Main.usersRegistry.findUser(newUserTextField.getText())) {
            warnTextLabel.setText("Nombre de usuario ya registrado.");
            return;
        }

        if(!newPasswordField.getText().equals(confirmPasswordField.getText())) {
            warnTextLabel.setText("Las contraseñas no coinciden.");
            return;
        }

        String[] data = {
            newUserTextField.getText(),
            newPasswordField.getText(),
            fullNameTextField.getText(),
            emailTextField.getText(),
            contactTextField.getText(),
            "0"
        };

        // Añade el usuario y escribe el archivo
        Main.usersRegistry.add(data);
        Main.usersRegistry.writeTo();

        Stage stage = (Stage) newUserTextField.getScene().getWindow();
        stage.close();
    }
}
