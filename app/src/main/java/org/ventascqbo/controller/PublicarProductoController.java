package org.ventascqbo.controller;

import org.ventascqbo.TableData;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import java.io.File;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

public class PublicarProductoController {
    private String[] newProductArray = new String[9];
    private ArrayList<TableData> allInOneList = new ArrayList<TableData>();
    private boolean hasNoPhoto = true;

    @FXML
    private Label warnLabel;

    @FXML
    private ChoiceBox<String> categoryChoiceBox;

    @FXML
    private TextArea descTextField;

    @FXML
    private TextField priceTextField;

    @FXML
    private ImageView productImageView;

    @FXML
    private TextField productNameTextField;

    @FXML
    void finishPostButton(ActionEvent event) {
        if(
            productNameTextField.getText().isBlank() ||
            priceTextField.getText().isBlank() ||
            descTextField.getText().isBlank()
        ) {
            warnLabel.setText("Falta completar todos los datos");
            return;
        }

        if(hasNoPhoto) {
            warnLabel.setText("Falta subir una foto del producto");
            return;
        }

        newProductArray[1] = productNameTextField.getText();
        newProductArray[4] = "$" + priceTextField.getText();
        newProductArray[5] = descTextField.getText();

        if(Common.editProductIndex < 0) {
            Common.myPostsList.add(new TableData(newProductArray));
        } else {
            Common.myPostsList.set(Common.editProductIndex, new TableData(newProductArray));
            Common.editProductIndex = -1;
        }

        Stage stage = (Stage) warnLabel.getScene().getWindow();
        stage.close();
    }

    @FXML
    void uploadPhotoButton(ActionEvent event) {
        Stage fileChooserStage = new Stage();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Abrir archivo");
        fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Imágenes", "*.png", "*.jpg", "*.webp"));
        File selectedFile = fileChooser.showOpenDialog(fileChooserStage);

        if(selectedFile != null) {
            productImageView.setImage(new Image("file:" + selectedFile.getPath()));
            hasNoPhoto = false;
        }
    }

    public void initialize() throws ParseException {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        // Valores por defecto / autogenerados
        newProductArray[2] = Common.categories[0];
        newProductArray[6] = Common.myDataTableData.getCol1();
        newProductArray[8] = "No";

        if(Common.editProductIndex < 0) {
            newProductArray[3] = formatter.format(date);
            newProductArray[7] = "No";

            // Genera un ID para el nuevo producto
            allInOneList.addAll(Common.myShoppingList);
            allInOneList.addAll(Common.tableArrayList);
            allInOneList.addAll(Common.myPostsList);
            int id = 1;
            for(TableData table : allInOneList) {
                int tableId = Integer.parseInt(table.getCol1());
                if(tableId >= id) {
                    id = tableId + 1;
                }
            }
            newProductArray[0] = Integer.toString(id);
        } else {
            newProductArray[0] = Common.myPostsList.get(Common.editProductIndex).getCol1();
            newProductArray[3] = Common.myPostsList.get(Common.editProductIndex).getCol4();
            newProductArray[7] = Common.myPostsList.get(Common.editProductIndex).getCol8();
        }

        // Añade ChoiceBox y deja seleccionada la primera opción por defecto
        categoryChoiceBox.getItems().addAll(Common.categories);
        categoryChoiceBox.getSelectionModel().selectFirst();

        categoryChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue ov, Number value, Number new_value) {
                // Indice de 'Common.categories'
                newProductArray[2] = Common.categories[new_value.intValue()];
            }
        });
    }
}
