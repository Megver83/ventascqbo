package org.ventascqbo;

/**
 * Crea filas de tablas para ser usadas por TableColumn
 */
public class TableData {
    private String[] columns;

    /**
     * Inicializa el objeto.
     *
     * @param columns Array de strings que componen una fila
     */
    public TableData(String[] columns) {
        this.columns = columns;
    }

    public String getCol1() {
        return columns[0];
    }

    public String getCol2() {
        return columns[1];
    }

    public String getCol3() {
        return columns[2];
    }

    public String getCol4() {
        return columns[3];
    }

    public String getCol5() {
        return columns[4];
    }

    public String getCol6() {
        return columns[5];
    }

    public String getCol7() {
        return columns[6];
    }

    public String getCol8() {
        return columns[7];
    }

    public String getCol9() {
        return columns[8];
    }

    /**
     * Cambia el valor de una determinada columna.
     *
     * @param index Índice del array columns, es decir, la columna de la fila
     * @param value String con el nuevo valor
     */
    public void set(int index, String value) {
        columns[index] = value;
    }
}
