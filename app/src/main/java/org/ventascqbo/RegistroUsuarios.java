package org.ventascqbo;
import org.ventascqbo.controller.Common;

public class RegistroUsuarios extends TextReader {
    /**
     * Llama al constructor de la superclase para hacer crear la matriz de datos.
     *
     * @param txtFile Ubicación del archivo de texto con los datos de usuario.
     */
    public RegistroUsuarios(String txtFile) {
        super(txtFile);
    }

    /**
     * Comprueba si existe el usuario, luego su contraseña.
     *
     * @param usuario El nombre de usuario.
     * @param pwd La contraseña.
     * @return true en caso de que esté todo correcto.
     */
    public boolean verify(String usuario, String pwd) {
        for(String[] datosUsuario : matrixArrayList) {
            if(datosUsuario[0].equals(usuario) && datosUsuario[1].equals(pwd)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Comprueba si existe el usuario.
     *
     * @param usuario El nombre de usuario.
     * @return true en caso de que exista.
     */
    public boolean findUser(String usuario) {
        for(String[] datosUsuario : matrixArrayList) {
            if(datosUsuario[0].equals(usuario)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna un objeto TableData con los datos públicos de usuario.
     *
     * @param usuario El nombre del usuario.
     * @return Una fila TableData con datos de usuario no-sensibles.
     */
    public TableData get(String usuario) {
        String[] userData = new String[5];

        for(int i = 0; i < matrixArrayList.size(); i++) {
            if(matrixArrayList.get(i)[0].equals(usuario)) {
                userData[0] = usuario;                    // Nombre de usuario
                userData[1] = matrixArrayList.get(i)[2];  // Nombre completo
                userData[2] = matrixArrayList.get(i)[3];  // Correo
                userData[3] = matrixArrayList.get(i)[4];  // Contacto
                userData[4] = matrixArrayList.get(i)[5];  // Cantidad de ventas
            }
        }

        TableData table = new TableData(userData);
        return table;
    }
}
