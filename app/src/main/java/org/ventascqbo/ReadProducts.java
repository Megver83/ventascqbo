package org.ventascqbo;

import java.util.ArrayList;

/**
 * Lee el archivo de productos.
 * Hereda de TextReader, básicamente lo mismo, pero con la opción de mostrar el ArrayList completo.
 */
public class ReadProducts extends TextReader {
    public ReadProducts(String txtFile) {
        super(txtFile);
    }

    public ArrayList<String[]> get() {
        return matrixArrayList;
    }
}
