# Taller 2: Ventas Coquimbo

Desarrollado por David Pizarro y Tomás Vargas. Puede ver [el enunciado del taller aquí](doc/Taller2.pdf).

## Compilación y ejecución

Para facilitar la construcción de nuestro software, utilizamos `gradle`. Para compilarlo y ejecutarlo, simplemente ejecute:

````
$ gradle run
````

O si no tiene `gradle` instalado, puede utilizar el wrapper:

````
$ ./gradlew run
````

Si usa Windows, ejecute el script `gradlew.bat` en su lugar.

### Archivo JAR

Para crear un archivo fat-jar, ejecute:

````
$ gradle shadowJar
````

Encontrará el archivo resultante en `app/build/libs/app-all.jar`.

Para ejecutar, por ejemplo, en Parabola/Arch Linux:

````
$ cd app
$ java --module-path /usr/lib/jvm/default/lib/javafx.base.jar:/usr/lib/jvm/default/lib/javafx.controls.jar:/usr/lib/jvm/default/lib/javafx.graphics.jar:/usr/lib/jvm/default/lib/javafx.fxml.jar --add-modules javafx.controls,javafx.fxml -jar build/libs/app-all.jar
````

Reemplace el directorio de los módulos de java donde se encuentren los de JavaFX.

**NOTA**: Ventas Coquimbo espera encontrar un directorio `data` con los archivos de texto respectivos desde el directorio donde se ejecuta. Por eso en el ejemplo se hace `cd app`.<br>
**NOTA 2**: Aunque en muchos sitios recomiendan usar simplemente `--module-path PATH_TO_JVM_LIB`, en el caso de Arch y sus paquetes oficiales de OpenJDK, es necesario especificar los módulos ([FS#71510](https://bugs.archlinux.org/task/71510)).

## Cómo funciona

### Software utilizado

* Scene Builder
* OpenJDK
* OpenJFX
* Gradle
* Umbrello

### Diagrama de clases

![Diagrama de clases, hecho con Umbrello](doc/ClassDiagram.jpg)

### Datos de usuario y productos

En el directorio `app/txt` se almacenan archivos de texto plano con toda la información a ser leída y escrita. Los productos tienen una ID, la cual es usada para obtener su información guardada como `app/txt/id/ID_PRODUCTO.txt`.

El layout de cada archivo es el siguiente:

````
RegistroUsuarios.txt
--------------------
Nombre de usuario;Contraseña;Nombre completo;Correo;Contacto;Cantidad de ventas
````
````
Productos.txt
-------------
ID;Nombre;Fecha;Precio;Descripción;Vendedor;Vendidos ("Si" o "No");Visto ("Si" o "No")
````
````
ID_PRODUCTO.txt
---------------
ID compra;Fecha;Comprador
````

Los textos proporcionados están a disposición a modo de ejemplo, es decir, puedes editarlos manualmente siguiendo los lineamientos mencionados.

## Bibliografía

Links de las guías, documentos y tutoriales que nos ayudaron a crear Ventas Coquimbo:

* https://www.geeksforgeeks.org/javafx-choicebox/
* https://github.com/eif-courses/javafx-gradle-fxml
* https://www.youtube.com/watch?v=pf7-zMCQlIc
* https://www.youtube.com/watch?v=DH3dWzmkT5Y
* https://docs.gradle.org/
* https://docs.oracle.com/javase/8/javase-clienttechnologies.htm
* https://openjfx.io/javadoc/18/
* https://stackoverflow.com/questions/27934627/how-to-show-default-choice-in-javafx-choicebox
* https://stackoverflow.com/questions/29338352/create-filechooser-in-fxml
* https://www.tutorialspoint.com/how-to-get-the-current-date-in-java
* https://www.geeksforgeeks.org/java-program-to-merge-two-arrays/
* https://en.wikipedia.org/wiki/Class_diagram
* https://en.wikipedia.org/wiki/Domain_model